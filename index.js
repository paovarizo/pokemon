const input = document.getElementById('isearch')
const search_btn = document.getElementById('b-search')

const connectApi = async term =>{
    const response = await fetch(`https://pokeapi.co/api/v2/pokemon/${term}`)
    const pokemonJson = await response.json()
    

    //consultas a la API para extraer los datos solicitados 
    document.getElementById('pokemon_name').innerHTML= "\nNombre del Pokemon:" + pokemonJson.name 
    document.getElementById('pokemon_char').innerHTML = "\nPeso del Pokemon:" +`${pokemonJson.weight}kg`
    document.getElementById('pokemon_height').innerHTML = "\nAltura del Pokemon:" +`0.${pokemonJson.height}m`
    document.getElementById('pokemon_img').setAttribute('src', pokemonJson.sprites.other.dream_world.front_default)

    var input_d = document.getElementById('isearch').value


    var infoInput = input_d

    hist.push(infoInput)
    console.log(hist)

}




//Sistema de registro de usuarios 

//arreglo donde se guardaran a los usuarios
var data = []
var hist= []

function registraUsuario(){
    var registerUsername = document.getElementById('name-create').value
    var registerEmail = document.getElementById('email-create').value
    var registerPassword= document.getElementById('pasw-create').value

    //definimos el objeto que ira en nuestro arreglo, el objeto contiene el username, email y password

    var infoUser={
        username: registerUsername,
        email: registerEmail,
        password: registerPassword
    }

    for (i=0;i<data.length;i++){
        if(registerUsername == data[i].username){
            alert('Este nombre de usuario ya esta registrado, intenta con otro')
            break
        } else if(registerPassword.length < 8){
            alert('Tu contraseña debe ser al menos de 8 caracteres')
            break
        }
    }

    data.push(infoUser)
    goHome()
    console.log(data)
    

}
function llevaHtml(){
    location.href="createAccount.html"
}

function togglePopup(){
    var modal = document.getElementById("myModal");
    var btn = document.getElementById("myBtn");
    var span = document.getElementsByClassName("close")[0];
    for (i=0; i<hist.length;i++){
        document.getElementById('input_data').innerHTML= "Pokemones buscados"+ "<br/>"+ hist[i]+ "<br/>"
    }
    btn.onclick = function() {
      modal.style.display = "block";
    }
    span.onclick = function() {
      modal.style.display = "none";
    }
    window.onclick = function(event) {
      if (event.target == modal) {
        modal.style.display = "none";
      }
    }
}

function showHistorial(){
    for (i=0; i<hist.length;i++){
        alert("Pokemones buscados"+ "<br/>"+ hist[i]+ "<br/>")
        
    }
}

function goHome(){
    location.href="index.html"
}

search_btn.addEventListener('click',() => connectApi(input.value) )


